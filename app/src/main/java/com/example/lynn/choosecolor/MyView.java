package com.example.lynn.choosecolor;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lynn on 3/12/2015.
 */

public class MyView extends LinearLayout implements AdapterView.OnItemSelectedListener {
    Spinner red;
    Spinner green;
    Spinner blue;
    MyCanvas myCanvas;

    public TextView createTextView(Context context,String label) {
       TextView view = new TextView(context);

       view.setText(label);

        return(view);
    }

    public MyView(Context context) {
        super(context);

        List<Integer> choices = new ArrayList<>();

        for (int counter=0;counter<256;counter++)
            choices.add(counter);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(context,android.R.layout.simple_list_item_1,choices);

        red = new Spinner(context);
        green = new Spinner(context);
        blue = new Spinner(context);

        red.setAdapter(adapter);
        green.setAdapter(adapter);
        blue.setAdapter(adapter);

        addView(createTextView(context,"Red"));
        addView(red);
        addView(createTextView(context,"Green"));
        addView(green);
        addView(createTextView(context,"Blue"));
        addView(blue);

        red.setOnItemSelectedListener(this);
        green.setOnItemSelectedListener(this);
        blue.setOnItemSelectedListener(this);

        myCanvas = new MyCanvas(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100,100);

        myCanvas.setLayoutParams(layoutParams);

        addView(myCanvas);
    }

    public void onItemSelected(AdapterView<?> parent,View v,int position,long id) {
        int red = Integer.parseInt(this.red.getSelectedItem().toString());
        int green = Integer.parseInt(this.green.getSelectedItem().toString());
        int blue = Integer.parseInt(this.blue.getSelectedItem().toString());

        Paint paint = new Paint();

        paint.setARGB(255,red,green,blue);

        myCanvas.draw(paint);
    }

    public void onNothingSelected(AdapterView<?> arg0) {

    }

}
