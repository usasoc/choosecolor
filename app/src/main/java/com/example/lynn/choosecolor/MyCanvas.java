package com.example.lynn.choosecolor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by lynn on 3/12/2015.
 */
public class MyCanvas extends View {
    private Paint paint;

    public MyCanvas(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFF000000);
    }

    public void draw(Paint paint) {
        this.paint = paint;

        invalidate();
    }

    public void onDraw(Canvas canvas) {
        canvas.drawRect(0,0,100,100,paint);
    }



}
